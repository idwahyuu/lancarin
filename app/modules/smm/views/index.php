<div class="wrap-content">
	<div class="">
		  	<div class="card">
		  		<div class="card-header">
		  			<div class="card-title">
                        <img src="<?=base_url('assets/img/logo_suntikfollowers.png?t=1');?>"> SMM Panel Suntikfollowers
                    </div>
                    <div class="clearfix"></div>
		  		</div>
		  		<div class="card-body">
					<div class="row " style="margin-bottom: 10px; padding: 0 15px;">
						<div class="col-md-12">
	  						<div class="bg-info" role="alert"><b>Info: </b> Akun anda tidak digunakan pada website ini. Silahkan kunjungi website <a class="" target="_blank" href="https://suntikfollowers.com"><u>suntikfollowers.com</u></a> untuk melakukan pemesanan</div>
	  					</div>
	  				</div>
	  				<div class="row" style="margin-bottom: 10px; padding: 0 15px;">
	  					<div class="col-md-12">
	  						<div class="table-responsive">
	  						<table class="table table-bordered table-hover table-striped" >
				                <thead>
				                    <tr>
				                    	<th>ID</th>
				                        <th>Nama</th>
				                        <th style="width:100px;">Harga</th>
				                        <th>Minimal</th>
				                        <th>Maksimal</th>
				                        <th>Layanan</th>
				                        <th>Tipe</th>
				                    </tr>
				                </thead>
				                <tbody>
				                	<?php if(empty($list)):?>
				                    <tr>
				                    	<td colspan="7">Tidak ada layanan</td>
				                    </tr>
				                    <?php else: foreach($list as $row):?>
				                    <tr><?=$row;?></tr>
				                    <?php endforeach;endif;?>
				                </tbody>
						  	</table>
						  	</div>
	  					</div>
	  				</div>
		  		</div>
		  	</div>
		</div>
	</div>
</div>

<script>
  $(function(){
	  function mynotif(title, msg, color='red'){
		iziToast.show({
			title: title,
			message: msg,
			color: color,
			position: 'bottomCenter'
		});
	}
	
	
	//mynotif('Perhatian', 'Halaman ini sedang dalam perbaikan, anda tidak akan dapat melakukan post sampai proses ini selesai.', 'blue');
	
	
  });
</script>