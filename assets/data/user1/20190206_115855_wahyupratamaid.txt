Id,Url,Caption
1964867513789347573_10575628124,https://www.instagram.com/p/BtEnJfuHnb1/,"Wanita yang selalu sabar ketika aku marah, selalu setia dengan bagaimanapun aku, selalu mendukung bagaimanapun aku, selalu menemani dan bersabar ketika aku sakit dan terpuruk, selalu mendoakan setiap langkahku, selalu mengerti, menghargai dan menyangiku adalah hanya satu-satunya wanita yang diciptakan tuhan untukku, yaitu ibuku. Love you mah."
1961229190016963591_10575628124,https://www.instagram.com/p/Bs3r46qFpQH/,"Bisnis vs Pertemanan

Bisnis dan Pertemanan, adalah 2 (dua) hal yang terkadang susah untuk dipisahkan.

Disaat harus mengambil keputusan, terkadang kita harus mempertimbangkan status pertemanan kita.

Namun, bagaimanapun kondisinya, dalam bisnis kita harus tetap menentukan keputusan yang adil secara logis tanpa mempedulikan status pertemanan.

Setelahnya, hanya ada 2 tipe teman yang menanggapi keputusan kita ;

1. Suka atau tidak suka dengan keputusan kita. Dengan ilmu yang dimiliki atau ilmu yang didapat dari kita digunakan untuk membuat sesuatu di lain bidang dan membuktikan kesuksesannya.

2. Tidak suka dengan keputusan kita. Dengan ilmu yang dimiliki atau ilmu yang didapat dari kita digunakan untuk membuat sesuatu di 'bidang yang sama' dengan kita. Dan berniat untuk menjatuhkan kita.

Kesimpulannya :

Tipe 1 : Orang yang mengambil makna positif dari sebuah keputusan yang diterima dan dijadikan pelajaran untuk memperoleh kehidupan yang lebih baik.

Tipe 2 : Orang yang didalam hatinya penuh kebencian dan iri hati. Tidak memiliki niat untuk mempelajari hal baru atau menerapkan ilmu yang dimiliki untuk membuat sesuatu yang baru.

So.. bagaimanapun tanggapan yang kita terima, kita tidak perlu terlalu peduli. Cukup lebih fokus dengan apa yang kamu kerjakan.

Percayalah, apa yang kamu tabur adalah apa yang akan kamu tuai.

Dan ketika kamu salah dalam mengambil keputusan, segera jadikan itu sebagai sebuah pelajaran dan segera menentukan solusinya. Stop complainin', do somethin' about it."
